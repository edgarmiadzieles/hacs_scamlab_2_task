import os, sys
#
from eml_adapter import EmlAdapter
from strategies import dictionary_based


def scan(dir, file):
    if file:
        scan_file(file)
    else:
        scan_dir(dir)


def scan_file(path):
    if not os.path.isfile(path):
        sys.exit("Provided path is not a file")

    adapter = EmlAdapter()
    adapter.parse(path)
    #dictionary_based.train()
    res = dictionary_based.run(adapter)
    print(res + ": " + path)


def scan_dir(path):
    if not os.path.isdir(path):
        sys.exit("Provided path is not a directory")

    adapter = EmlAdapter()
    for eml_file in os.listdir(path):
        if eml_file.endswith('.eml'):
            eml_file_path = os.path.join(path, eml_file)
            adapter.parse(eml_file_path)

            res = dictionary_based.run(adapter)
            print(res + ": " + eml_file_path)