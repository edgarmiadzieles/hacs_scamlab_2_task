import sys, getopt

import autoreply
from filter import scan


def main(argv):
    action_dir = ""
    action_file = ""
    action = ""
    text = ""

    try:
        opts, args = getopt.getopt(argv, " d:f:a:t:", ["dir=", "output-file=", "file=", "action=", "text="])
    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    for opt, arg in opts:
        if opt in ["-d", "--dir"]:
            action_dir = arg
        elif opt in ["-o", "--output-file"]:
            sys.stdout = open(arg, 'w')
        elif opt in ["-f", "--file"]:
            action_file = arg
        elif opt in ["-a", "--action"]:
            action = arg
        elif opt in ["-t", "--text"]:
            text = arg

    if action == "sender" and action_file:
        autoreply.get_sender(action_file)

    if action == "segregate":
        scan(action_dir, action_file)
    elif action == "reply" and action_file:
        autoreply.reply(action_file)
    elif action == "reply" and text:
        autoreply.reply_text(text)
    else:
        sys.exit("Choose action segregate or reply")


if __name__ == '__main__':
    main(sys.argv[1:])
