import os

from helper import extract_words
from itertools import groupby

# https://pypi.org/project/pandas/
import pandas as pd
import re

# https://pypi.org/project/scikit-learn/
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression

# https://pypi.org/project/joblib/
from joblib import dump, load


def scam_score(eml_adapter):
    text = extract_words_from_eml(eml_adapter)

    with open(os.path.dirname(__file__) + "/scam_word_list.txt") as f:
        scam_phrases = f.readlines()
    scam_phrases = [x.strip().lower() for x in scam_phrases]

    found_phrases = []
    for i in range(3):
        found_phrases += find_phrases(text, scam_phrases, i)
    found_phrases.sort()
    found_phrases = [{key, len(list(group))} for key, group in groupby(found_phrases)]
    print(found_phrases)

    return extract_words_from_eml(eml_adapter)


def extract_words_from_eml(eml_adapter):
    body = eml_adapter.email.get('body')[0].get('content')
    subject = eml_adapter.email.get('header').get('subject')
    return extract_words(body) + " " + extract_words(subject)


def find_phrases(text, scam_phrases, by):
    words = re.findall(r"[\w']+", text)
    phrases = []

    while words:
        word = ""
        for i in range(by):
            if len(words) <= i:
                continue
            word = (word + " " + words[i]).strip()
        phrases.append(word)
        words.pop(0)

    found_phrases = []
    for phrase in phrases:
        if phrase in scam_phrases:
            found_phrases.append(phrase)

    return found_phrases

def run(eml_adapter):
    text = extract_words_from_eml(eml_adapter)
    model = load('finalized_model.sav')

    d = {'label': [0], 'text': [text]}
    df = pd.DataFrame(data=d)

    v = load('finalized_countvectorizer.sav')
    train_df = load('finalized_train_df.sav')

    test_df = v.transform(df['text'])

    predictions = model.predict(test_df)
    return "scam" if predictions[0] == 1 else "not scam"



def train():
    messages = pd.read_csv("spam_or_ham.csv")
    messages['label'].value_counts()
    messages['label'] = messages['label'].map({'ham': 0, 'spam': 1})

    X_train, X_test, y_train, y_test = train_test_split(messages["text"], messages["label"], test_size=0.1, random_state=10, shuffle=True)
    v = CountVectorizer(analyzer='word', ngram_range=(2, 2))
    v.fit(X_train)

    train_df = v.transform(X_train)

    model = LogisticRegression()
    model.fit(train_df, y_train)

    filename = 'finalized_model.sav'
    dump(model, filename)

    filename = 'finalized_countvectorizer.sav'
    dump(v, filename)

    filename = 'finalized_train_df.sav'
    dump(v, filename)

