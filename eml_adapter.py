# https://pypi.org/project/eml-parser/
import eml_parser


class EmlAdapter:

    def __init__(self):
        self.ep = eml_parser.EmlParser(include_raw_body=True, include_attachment_data=True)
        self.email = None

    def parse(self, file):
        with open(file, 'rb') as fhdl:
            raw_email = fhdl.read()

        self.email = self.ep.decode_email_bytes(raw_email)