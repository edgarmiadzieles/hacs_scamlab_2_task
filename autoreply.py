import os
import sys
import random
import spacy
import pytextrank

from eml_adapter import EmlAdapter
from strategies.dictionary_based import extract_words_from_eml

def rank(text):
    nlp = spacy.load("en_core_web_sm")
    tr = pytextrank.TextRank()
    nlp.add_pipe(tr.PipelineComponent, name="textrank", last=True)
    return nlp(text)


def reply(path):
    if not os.path.isfile(path):
        sys.exit("Provided path is not a file")

    adapter = EmlAdapter()
    adapter.parse(path)
    text = extract_words_from_eml(adapter)
    reply_text(text)

def get_sender(path):
    if not os.path.isfile(path):
        sys.exit("Provided path is not a file")

    adapter = EmlAdapter()
    adapter.parse(path)
    to=adapter.email.get("header")["from"]
    if type(to) == list:
        to = to[0]
    print(to)


def reply_text(text):
    ranked = rank(text)
    return gen_en_response(ranked)


def random_reply():
    return random.choice([
        "Sorry!!!! this is not my first langage. what you mean by *.",
        "I don't understand. What do you mean by *.",
        "oooooooh! plase tell more about *.",
        "this is intresting. how does * work?",
        "thank you for * opportunity, can you tell more about *",
        "wow!! * sound so cool!!!!! thank you! I am intrested!",
    ])


def gen_en_response(ranked):
    response = random_reply()
    for p in ranked._.phrases:
        if "*" in response:
            response = response.replace("*", p.text, 1)
        else:
            break
    print(response)