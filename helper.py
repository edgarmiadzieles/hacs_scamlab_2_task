# https://pypi.org/project/text2html/
import html2text

# https://pypi.org/project/functools/
import functools


def extract_words(raw):
    ht = html2text.HTML2Text()
    ht.ignore_images = ht.ignore_links = True
    return ' '.join(ht.handle(raw).lower().split())


def compose(*functions):
    return functools.reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)


def split_words(string, split_size):
    return 0