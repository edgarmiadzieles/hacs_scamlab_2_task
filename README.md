### Description
Trained model with skikit-learn based on about 1000 legit and 1000 spam messages.
Video of usage included, but the usage is pretty straightforward.

### Usage
  * Download required libraries (referenced in code)
  * `python main.py -f 'filename' -a segregate` for file
  * `python main.py -d 'dir' -a segregate` for directory
  * `-o 'output'` to change stdout to file

### HACS 6 usage
  * Download required libraries (referenced in code)\
  * Make sure to install en_core_web_sm with `python -m spacy download en_core_web_sm`
  * `python main.py -f 'filename' -a reply` to get response
  * I tryed to make an email server, but after a lot of testing I think the spam score is just through the roof. Anyway, here is how you can test it
  * ```
    telnet 207.154.225.176 25
    EHLO box.mailserver.mailer
    MAIL From: <youtemail>
    RCPT To: julia@mailserver.mailer
    DATA
    From: <youtemail>
    To: julia@mailserver.mailer
    Subject: Testing
    
    Email contents
    ```
  * press `.`
  * type `quit` to exit telnet